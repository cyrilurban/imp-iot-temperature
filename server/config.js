/**
 * @description Config of application.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 18.12.2017
 */

var path = require('path');

/**
 * @summary Config of application. Using path for get filename.
 * @function conf
 * @static
 */
module.exports.conf = {
	"app": {
		"port": 3000
	},
	"secure": {
		"auth_code": "impsecurity"
	},
	"db": {
		"type": "sqlite3",
		"filename": path.join(__dirname, './db.sqlite3')
	}
}
