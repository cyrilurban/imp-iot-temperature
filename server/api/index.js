/**
 * @description index for API.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 18.12.2017
 */

const express = require("express");
const router = express.Router();

router.use("/", require("./temp.js"));

module.exports = router;
