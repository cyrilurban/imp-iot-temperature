/**
 * @description API for temp.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 18.12.2017
 */

const express = require("express");
const router = express.Router();
const temp = require('./../db/temp.js');
const config = require('./../config.js');

/**
 * @summary Save last temp of sensor
 * @param {Object} req request
 * @param {Object} res response
 * @function save
 * @static
 * @private
 */
var save = function (req, res) {
	// wrong arguments
	if (req.body.auth == undefined ||
		req.body.mac == undefined || req.body.temperature == undefined)
	{
		res.status(400).send("Wrong arguments!");
	}
	// authentication
	else if (req.body.auth != config.conf.secure.auth_code) {
		res.status(401).send("Unauthorized, wrong auth. code!");
	}
	// correct
	else {
		temp.save(req.body.mac, req.body.temperature)
		.then(data => {
			console.log(data);
			// send OK
			res.status(200).send();
		})
		.catch(e => {
			console.error(e);
			// send error status and error msg
			res.status(e.status).send(e.msg);
		});
	}
}

/**
 * @summary Get all Contacts (or only surnames).
 * @param {Object} req request
 * @param {Object} res response
 * @function save
 * @static
 * @private
 */
var getSensors = function (req, res) {
	temp.getSensors()
	.then(data => {
		console.log(data);
		res.json(
			data
		);
	})
	.catch(e => {
		console.error(e);
		res.status(500).send("Internal Server Error during get contact from DB");
	});
}

/**
 * [get description]
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
var get = function (req, res) {
	temp.get(req.body.sensor)
	.then(data => {
		res.json(
			data
		);
	})
	.catch(e => {
		console.error(e);
		res.status(500).send("Internal Server Error during get contact from DB");
	});
}

/**
 * @summary Get temp
 * @function contact
 * @private
 */
router.post("/get", get);

/**
 * @summary Get all sensors
 * @function contact
 * @private
 */
router.get("/sensors", getSensors);

/**
 * @summary Save currently temp.
 * @function contact
 * @private
 */
router.post("/", save);

module.exports = router;
