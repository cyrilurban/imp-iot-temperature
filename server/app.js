/**
 * @description Init server.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 18.12.2017
 */

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const config = require('./config.js');

// middleware for allow to connect from remote web client
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});

// body parser - accept json request
app.use(bodyParser.json());

// services
app.use("/", require("./api"));

app.listen(config.conf.app.port);
console.log("Listening on port " + config.conf.app.port);

module.exports = app;
