/**
 * @description Functions for saving and geting data from DB for temp.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 18.12.2017
 */

const knex = require("./init.js");

/**
 * @summary Save currently temp. to DB.
 * @returns {Promise} resolve:, reject: {Object} error
 * @function save
 * @public
 * @static
 */
var save = function (mac, temp) {
	var table;
	var now;
	return getTable(mac)
	.then(tab => {
		table = tab;
		now = new Date;
		return knex
		.insert({
			"time": now,
			"temp": temp
		})
		.into(table);
	})
	.then(id => {
		return Promise.resolve(table + ": " + temp + "°C [" +
			now.toISOString() + "]");
	})
	.catch(e => {
		console.error(e);
		var error = {};
		error.staus = 500;
		error.msg = e;
		return error;
	});
};

/**
 * @summary Get array of sensors.
 * @returns {Promise} resolve: {Array} all sensors, reject: {Object} error
 * @function getSensors
 * @public
 * @static
 */
var getSensors = function () {
	return knex
	.select()
	.column("ID")
	.from("sensors")
	.then(rows => {
		var names = [];
		if (rows.length != undefined && rows.length != 0) {
			for (var i = 0; i < rows.length; i++) {
				names.push("sensor" + rows[i].ID);
			}
		}
		return names;
	})
	.catch(e => {
		console.error(e);
		return e;
	});
};

/**
 * @summary POST last (max) 20 temp. of sensor name
 * @returns {Promise} resolve: {Object} times and temps, reject: {Object} error
 * @function get
 * @public
 * @static
 */
var get = function (sensor) {
	console.log("POST temperature of " + sensor);
	return knex
	.select()
	.column("time", "temp")
	.from(sensor)
	.orderBy("ID", "desc")
	.limit(20)
	.then(rows => {
		if (rows.length != undefined && rows.length != 0) {
			for (var i = 0; i < rows.length; i++) {
				rows[i].time = parseFloat(rows[i].time);
				rows[i].temp = parseFloat(rows[i].temp);
			}
		}
		return rows;
	})
	.catch(e => {
		console.error(e);
		return e;
	});
};

/**
 * @summary Get name of table with mac.
 * @param {string} mac mac adress
 * @returns {Promise} resolve:, reject: {Object} error
 * @function getTable
 * @private
 * @static
 */
var getTable = function (mac) {
	return knex
	.select()
	.column("ID")
	.from("sensors")
	.where("mac", "=", mac)
	.then(rows => {
		if (rows.length == undefined || rows.length == 0) {
			console.log("new");
			return newSensor(mac);
		} else {
			return "sensor" + rows[0].ID;
		}
	})
	.catch(e => {
		console.error(e);
		return e;
	});
};

/**
 * @summary Create new table (and new record in sensors).
 * @param {string} mac mac adress
 * @returns {Promise} resolve:, reject: {Object} error
 * @function getTable
 * @private
 * @static
 */
var newSensor = function (mac) {
	var table_name;

	// add record to table sensors
	return knex
	.insert({
		"mac": mac
	}, "ID")
	.into("sensors")
	.then(id => {
		// create new table
		table_name = "sensor" + id;
		return knex.schema
		.createTableIfNotExists(table_name, function (table) {
			table.increments("ID");
			table.string("time");
			table.string("temp");
		});
	})
	.then(table => {
		// return table name
		return table_name;
	});
};

module.exports = {
	save: save,
	getSensors: getSensors,
	get: get
};
