/**
 * @description Init DB.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 18.12.2017
 */

const config = require('./../config.js');

/**
 * @summary Init DB, configuration require from config
 * @description Turn on check foreign keys.
 * @function knex
 * @static
 */
var knex = require('knex')({
	client: config.conf.db.type,
	connection: {
		filename: config.conf.db.filename
	},
	pool: {
		afterCreate: (conn, cb) => {
			conn.run('PRAGMA foreign_keys = ON', cb)
		}
	}
});

module.exports = knex;
