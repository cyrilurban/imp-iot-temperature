#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>

OneWire oneWire(D1);
DallasTemperature DS18B20(&oneWire);
/*
Needed create hot-spot on the notebook (Internet not necessery),
get hot-spot ssid and password,
look what is notebook IP (ipconfig for example),
start (node) server (on some port, 3000 for example)
*/
const char* ssid = "Connectify-imp";
const char* password = "imppassword";
String ip = "192.168.137.1";
String port = "3000";
String auth_code = "impsecurity";
String ip_port;
String mac;

String getTemperature() {
  float temp;
  do {
    DS18B20.requestTemperatures();
    temp = DS18B20.getTempCByIndex(0);
    delay(100);
  } while (temp == 85.0 || temp == (-127.0));

  char temperatureString[6];
  dtostrf(temp, 2, 2, temperatureString);

  return temperatureString;
}

void setup() {
  // serial connection
  Serial.begin(115200);
  // WiFi connection                            
  WiFi.begin(ssid, password);

  // wait for the WiFI connection completion
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Waiting for connection");
  }

  // prepare http ip port string
  ip_port = "http://";
  ip_port += ip;
  ip_port += ":";
  ip_port += port;
  ip_port += "/";

  mac = WiFi.macAddress();
}

void loop() {
  if (WiFi.status() == WL_CONNECTED) {
    // declare object of class HTTPClient
    HTTPClient http;
    http.begin(ip_port);
    http.addHeader("Content-Type", "application/json");

    // Prepare POST json msg
    String temperature = getTemperature();   
    String json_msg = "{\"temperature\":\"";
    json_msg += temperature;
    json_msg += "\", \"mac\":\"";
    json_msg += mac;
    json_msg += "\", \"auth\":\"";
    json_msg += auth_code;
    json_msg += "\"}";

    // Send the request
    int httpCode = http.POST(json_msg);
    // Get the response payload    
    String payload = http.getString();

    // Print HTTP return code
    Serial.println(httpCode);
    // Print response payload
    Serial.println(payload);

    // Close connection
    http.end();  
  } else {
    Serial.println("Error in WiFi connection");
  }

  // Send a request every 3 seconds
  delay(3000);
}
