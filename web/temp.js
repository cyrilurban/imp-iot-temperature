/**
 * @description JS for insurance.html
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 26.11.2017
 */

window.onload = function () {
	getAllSensors();

	var dps = []; // dataPoints
	var chart = new CanvasJS.Chart("chartContainer", {
		title :{
			text: "senzor1"
		},
		axisY: {
			includeZero: false
		},
		data: [{
			type: "line",
			dataPoints: dps
		}]
	});

	var xVal = 0;
	var yVal = 100;
	var updateInterval = 1000;
	var dataLength = 20; // number of dataPoints visible at any point

	var updateChart = function (count) {

		count = count || 1;

		for (var j = 0; j < count; j++) {
			yVal = yVal +  Math.round(5 + Math.random() *(-5-5));
			dps.push({
				x: xVal,
				y: yVal
			});
			xVal++;
		}

		if (dps.length > dataLength) {
			dps.shift();
		}

		chart.render();
	};

	updateChart(dataLength);
	setInterval(function(){updateChart()}, updateInterval);

}

// async get for check get All sencores
var getAllSensors = function () {
	$.ajax ({
		url: "http://localhost:3000/sensors",
		type: "GET",
		dataType: 'json',
		contentType: "application/json; charset=utf-8",
		success: function(sensors) {
			for (var i = 0; i < sensors.length; i++) {
				$('#sens')
				.append($("<option></option>")
				.attr("value", sensors[i])
				.text(sensors[i]));
			}
		}
	});
};
